module go-term-example

go 1.20

require (
	github.com/buger/goterm v1.0.4
	github.com/eiannone/keyboard v0.0.0-20220611211555-0d226195f203
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	golang.org/x/sys v0.9.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
