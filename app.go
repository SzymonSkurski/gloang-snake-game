package main

import (
	"fmt"
	"math/rand"
	"strings"
	"sync"
	"time"

	tm "github.com/buger/goterm"
)

type SnakeApp struct {
	// have to store fixed terminal dimension to avoid changes during play
	TWidth        int
	THeight       int
	Debug         bool
	startUnixNano int64
	snake         *Snake
	prevFrame     int64
	frameRate     int
	isRunning     bool
	isInit        bool
	game          Game
	score         int
	gameCounter   int
	mutex         sync.Mutex
}

func NewSnakeAPP() *SnakeApp {
	return &SnakeApp{
		TWidth:  tm.Width(),
		THeight: tm.Height(),
		mutex:   sync.Mutex{},
	}
}

func (app *SnakeApp) Start() {
	app.mutex.Lock()

	app.frameRate = frameRate
	app.prevFrame = 0
	app.score = 0
	vX, vY := app.getStartVectors() // random vectors
	app.snake = NewSnake(float64(app.TWidth)/2, float64(app.THeight)/2, float64(vX), float64(vY))
	app.MakeNewGame()
	app.startUnixNano = time.Now().UnixNano()
	app.isRunning = true
	app.gameCounter = 0

	app.mutex.Unlock()
}

func (app *SnakeApp) Restart() {
	if app.isRunning {
		return
	}

	tm.Clear()
	tm.Flush()
	app.Start()
}

func (app *SnakeApp) RunApp() {
	for {
		if !app.isInit {
			app.PrintKeys()
			continue
		}

		frame := app.getCurrentFrame()
		if !app.nextFrameReached(frame) {
			continue // only at full frame
		}
		// store current frame as prev frame
		app.prevFrame = frame

		app.action() // move snake
		app.check()  // check collision
		app.print()
	}
}

func (app *SnakeApp) nextFrameReached(frame int64) bool {
	return frame > app.prevFrame
}

func (app *SnakeApp) getCurrentFrame() int64 {
	if !app.isRunning {
		return 0
	}
	et := time.Now().UnixNano() - app.startUnixNano
	perS := float64(time.Second / time.Duration(app.frameRate))
	f := int64(float64(et) / perS)

	// app.printMiddleBottom(fmt.Sprintf("et:%d perS:%.2f f:%d pf:%d", et/int64(time.Second), perS, f, app.prevFrame), 0, 1)
	// tm.MoveCursor(app.TWidth, app.THeight)
	// tm.Flush()

	if f < 1 {
		return 1
	}
	return f
}

func (app *SnakeApp) newGameRandomXYC() XYCoordinate {
	margin := 3
	maxX := app.TWidth - margin
	minX := margin
	maxY := app.THeight - margin
	minY := margin

	x := rand.Intn(maxX-minX+1) + minX
	y := rand.Intn(maxY-minY+1) + minY

	c := *NewXyCoordinate(x, y)
	if app.snake.HasCollisionWith(c) {
		return app.newGameRandomXYC()
	}
	return c
}

func (app *SnakeApp) getStartVectors() (vX int, vY int) {
	vX = getRandomVector()
	if vX != 0 {
		return
	}
	for vY == 0 {
		vY = getRandomVector()
	}
	return
}

func (app *SnakeApp) action() {
	if !app.isRunning {
		return
	}
	// move snake forward
	app.snake.Move(app.TWidth, app.THeight)
	// game going to evade snake
	app.game.Evade(app.snake, app.TWidth, app.THeight)
	// move game
	app.game.Move(app.TWidth, app.THeight)
}

func (app *SnakeApp) check() {
	if !app.isRunning {
		return
	}
	if app.snake.HasCollisionWithSelf() {
		app.isRunning = false
		return
	}

	// check collision with game
	if app.snake.HasCollisionWith(*app.game.GetCoordinate()) {
		s := 1
		if app.score != 0 && (app.gameCounter+1)%len(app.gamesList()) == 0 {
			s = 5 // 5 points for diamond
		}
		app.score += s
		app.gameCounter++
		app.snake.Grow()
		app.snake.SpeedUp()
		app.MakeNewGame()
	}

	// game is gone
	if app.game.IsGone() {
		app.MakeNewGame()
	}
}

func (app *SnakeApp) MakeNewGame() {
	vx, vy := app.getStartVectors()
	s := float64(0)
	maxS := app.snake.speed / 2

	if app.score > 1 {
		s = float64(app.score) / 500
		if s > maxS {
			s = maxS
		}
	}

	app.game = *NewGame(app.newGameRandomXYC(), float64(vx), float64(vy), s)
}

func (app *SnakeApp) print() {
	tm.Clear() // Clear current screen
	app.printBoard()
	app.printDebug()
	app.printScore()
	app.printSnake()
	app.printGame()
	app.printGameCounter()
	app.printGameOver()
	tm.MoveCursor(app.TWidth, app.THeight)
	tm.Flush()
}

func (app *SnakeApp) printBoard() {
	if !app.isRunning {
		return
	}
	color := "\033[033m"
	// horizontal lines ▲ ▼
	for i := 1; i < app.TWidth-1; i += 2 {
		tm.MoveCursor(i, 0)
		_, _ = tm.Print(color, "▲", "\033[0m")
		tm.MoveCursor(i, app.THeight)
		_, _ = tm.Print(color, "▼", "\033[0m")
	}
	// vertical lines ◄ ►
	for i := 0; i < app.THeight; i++ {
		tm.MoveCursor(0, i)
		_, _ = tm.Print(color, "◄", "\033[0m")
		tm.MoveCursor(app.TWidth, i)
		_, _ = tm.Print(color, "►", "\033[0m")
	}
}

func (app *SnakeApp) printGame() {
	if !app.isRunning {
		return
	}
	blinkT := 25
	if stg := app.game.SecondsToGone(); (stg <= 0) || (app.game.IsAboutToGone() && int64(app.prevFrame)%((int64(stg+blinkT))*2) > (int64(stg+blinkT))) {
		return // pulsing
	}
	games := app.gamesList()
	char := games[app.gameCounter%len(games)]
	tm.MoveCursor(app.game.GetCoordinate().XY())
	tm.Print(tm.Color(char, tm.GREEN))
}

func (app *SnakeApp) printGameCounter() {
	if !app.isRunning {
		return
	}
	char := fmt.Sprint("  ", app.game.SecondsToGone(), "  ")
	tm.MoveCursor(app.TWidth-len([]rune(char)), 0)

	color := tm.GREEN
	// how many percent of time to leave has left
	pTL := float64(app.game.SecondsToGone()) / gameTTL * 100
	switch {
	case pTL <= 10:
		color = tm.RED
	case pTL <= 40:
		color = tm.YELLOW
	}

	tm.Print(tm.Background(tm.Color(char, color), tm.BLACK))
}

func (app *SnakeApp) gamesList() []string {
	return []string{"Œ", "&", "@", "%", "¥", "£", "$", "~", "œ", "§", "ð", "€", "❖"}
}

func (app *SnakeApp) printGameOver() {
	if app.isRunning {
		return
	}
	app.printMiddle("GAME OVER")
	app.printMiddle("press r to restart esc to exit", 0, 2)
}

func (app *SnakeApp) printMiddle(msg string, off ...int) {
	offset := make([]int, 2)
	copy(offset, off)
	tm.MoveCursor(app.TWidth/2-len([]rune(msg))/2+offset[0], app.THeight/2+offset[1])
	tm.Print(msg)
}

func (app *SnakeApp) printMiddleTop(msg string, off ...int) {
	offset := make([]int, 2)
	copy(offset, off)
	tm.MoveCursor(app.TWidth/2-len([]rune(msg))/2+offset[0], offset[1])
	tm.Print(msg)
}

func (app *SnakeApp) printMiddleBottom(msg string, off ...int) {
	offset := make([]int, 2)
	copy(offset, off)
	tm.MoveCursor(app.TWidth/2-len([]rune(msg))/2+offset[0], app.THeight+offset[1])
	tm.Print(msg)
}

func (app *SnakeApp) printSnake() {
	if !app.isRunning {
		return
	}
	// print head
	h := app.snake.getHeadCoordinate()
	tm.MoveCursor(h.X, h.Y)
	tm.Print("o")
	// print tail
	for i, t := range app.snake.Tail {
		char := "o"
		if i%5 == 0 {
			char = "ø"
		}
		tm.MoveCursor(t.X, t.Y)
		tm.Print(char)
	}
}

func (app *SnakeApp) printDebug() {
	if !app.Debug || !app.isRunning {
		return
	}
	dx, dy := app.snake.GetCoordinate().GetDistanceXYCrossBorders(app.TWidth, app.THeight, *app.game.GetCoordinate())

	color := "\033[031m"
	tm.Print(color)
	msg := fmt.Sprint("dx:", dx, " dy:", dy, "  ", app.snake.DebugMsg(), "  ")
	tm.MoveCursor(app.TWidth-len([]rune(msg)), app.THeight)
	tm.Print(tm.Background(msg, tm.BLACK))
	tm.Print("\033[0m") // reset
}

func (app *SnakeApp) printScore() {
	app.printMiddleTop(fmt.Sprintf("score:%v", app.score))
}

func (app *SnakeApp) PrintKeys() {
	tm.Clear()
	app.printMiddle("Welcome to Snake game!", 0, -7)
	app.printMiddle("esc - quit game", 0, -3)
	app.printMiddle("d - toggle debug", 0, -2)
	app.printMiddle("arrows ←→ ↑↓ - control snake", 0, -1)
	app.printMiddle("hunt games to get score", 0, 1)
	app.printMiddle(strings.Join(app.gamesList()[:len(app.gamesList())-1], " "), 0, 2)
	app.printMiddle("diamonds ❖ are for 5 pts", 0, 3)

	app.printMiddle("press any key to start", 0, 5)
	tm.MoveCursor(app.TWidth+1, app.THeight+1)
	tm.Flush()
}
