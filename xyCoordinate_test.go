package main

import (
	"testing"
)

func TestGetDistanceXYTo(t *testing.T) {
	testCases := []struct {
		coordinate       *XYCoordinate
		targetCoordinate *XYCoordinate
		expectedX        int
		expectedY        int
	}{
		{
			coordinate:       NewXyCoordinate(0, 0),
			targetCoordinate: NewXyCoordinate(0, 0),
			expectedX:        0,
			expectedY:        0,
		},
		//  0123456789
		// 0    s.....
		// 1
		// 2         t
		// sx: 4, sy: 0, tx: 9, ty: 2
		{
			coordinate:       NewXyCoordinate(4, 0),
			targetCoordinate: NewXyCoordinate(9, 2),
			expectedX:        5,
			expectedY:        2,
		},
		//  0123456789
		// 0    t.....
		// 1
		// 2         s
		// sx: 9, sy: 2, tx: 4, ty: 0
		{
			coordinate:       NewXyCoordinate(9, 2),
			targetCoordinate: NewXyCoordinate(4, 0),
			expectedX:        5,
			expectedY:        2,
		},
	}

	for _, tc := range testCases {
		rx, ry := tc.coordinate.GetDistanceXYTo(*tc.targetCoordinate)

		if rx != tc.expectedX {
			msg := "from sx: %d to tx: %d expected distance x: %d, but got %d instead"
			t.Errorf(msg, tc.coordinate.X, tc.targetCoordinate.X, tc.expectedX, rx)
		}

		if ry != tc.expectedY {
			msg := "from sy: %d to ty: %d expected distance y: %d, but got %d instead"
			t.Errorf(msg, tc.coordinate.Y, tc.targetCoordinate.Y, tc.expectedY, ry)
		}
	}

}

func TestGetGetDistanceToBorders(t *testing.T) {
	testCases := []struct {
		width      int
		height     int
		coordinate *XYCoordinate
		expected   []int // l,r,t,b int
	}{
		//  0123456789
		// 0c.........
		// 1..........
		// 2..........
		{
			width:      10,
			height:     10,
			coordinate: NewXyCoordinate(0, 0),
			expected:   []int{0, 10, 0, 10},
		},
	}

	for _, tc := range testCases {
		rl, rr, rt, rb := tc.coordinate.GetDistanceToBorders(tc.width, tc.height)

		el := tc.expected[0]
		er := tc.expected[1]
		et := tc.expected[2]
		eb := tc.expected[3]

		if el != rl {
			t.Errorf("expected l: %d but go %d instead", el, rl)
		}

		if er != rr {
			t.Errorf("expected r: %d but go %d instead", er, rr)
		}

		if et != rt {
			t.Errorf("expected t: %d but go %d instead", et, rt)
		}

		if eb != rb {
			t.Errorf("expected b: %d but go %d instead", eb, rb)
		}
	}

}

func TestGetGetDistanceCrossBorders(t *testing.T) {
	testCases := []struct {
		width      int
		height     int
		coordinate *XYCoordinate
		target     *XYCoordinate
		expectedDX int
		expectedDY int
	}{
		//  0123456789
		// 0s.........
		// 1..........
		// 2..........
		{
			width:      10,
			height:     10,
			coordinate: NewXyCoordinate(0, 0),
			target:     NewXyCoordinate(0, 0),
			expectedDX: 0,
			expectedDY: 0,
		},
		//  0123456789
		// 0s.........
		// 1..........
		// 2.........t
		{
			width:      10,
			height:     10,
			coordinate: NewXyCoordinate(0, 0),
			target:     NewXyCoordinate(10, 2),
			expectedDX: 1,
			expectedDY: 2,
		},
		//  0123456789
		// 0t.........
		// 1..........
		// 2.........s
		{
			width:      10,
			height:     10,
			coordinate: NewXyCoordinate(10, 2),
			target:     NewXyCoordinate(0, 0),
			expectedDX: 1,
			expectedDY: 2,
		},
		//  0123456789
		// 0..........
		// 1..........
		// 2...s......
		// 3..........
		// 4.....t....
		// 5..........
		// 6..........
		{
			width:      10,
			height:     10,
			coordinate: NewXyCoordinate(3, 2),
			target:     NewXyCoordinate(5, 4),
			expectedDX: 2,
			expectedDY: 2,
		},
		//  0123456789
		// 0..........
		// 1..........
		// 2...s......
		// 3..........
		// 4..........
		// 5..........
		// 6..........
		// 7..........
		// 8..........
		// 9.....t....
		{
			width:      10,
			height:     10,
			coordinate: NewXyCoordinate(3, 2),
			target:     NewXyCoordinate(5, 9),
			expectedDX: 2,
			expectedDY: 4,
		},
	}

	for _, tc := range testCases {
		dx, dy := tc.coordinate.GetDistanceXYCrossBorders(tc.width, tc.height, *tc.target)

		if dx != tc.expectedDX {
			msg := "expected distanceX: %d but got %d"
			t.Errorf(msg, tc.expectedDX, dx)
		}

		if dy != tc.expectedDY {
			msg := "expected distanceY: %d but got %d"
			t.Errorf(msg, tc.expectedDY, dy)
		}
	}
}
