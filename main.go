package main

const (
	frameRate   = 50            // how many frames per s
	speed       = float64(0.15) // start speed
	snakeLength = 5             // start length
	gameTTL     = 25            // how many seconds before game will go
)

// var app = NewSnakeAPP()

func main() {
	app := NewSnakeAPP()

	go keyHandler(app) // key listener and handler

	app.Start()
	app.RunApp()

}
