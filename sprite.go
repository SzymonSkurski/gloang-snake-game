package main

type Sprite struct {
	x       float64
	y       float64
	vectorX float64
	vectorY float64
	speed   float64
}

func NewSprite(x, y, vx, vy, speed float64) *Sprite {
	s := &Sprite{
		x:       x,
		y:       y,
		vectorX: vx,
		vectorY: vy,
		speed:   speed,
	}

	return s
}

func (s *Sprite) Stop() {
	s.vectorX = 0
	s.vectorY = 0
}

func (s *Sprite) GoUp() {
	if s.vectorY == -1 {
		return // going up already
	}

	if s.vectorY == 1 {
		return // prevent suicide
	}
	s.vectorY = -1
	s.vectorX = 0
}

func (s *Sprite) Godown() {
	if s.vectorY == 1 {
		return // going down already
	}

	if s.vectorY == -1 {
		return // prevent suicide
	}
	s.vectorY = 1
	s.vectorX = 0
}

func (s *Sprite) GoLeft() {
	if s.vectorX == -1 {
		return // going left already
	}

	if s.vectorX == 1 {
		return // prevent suicide
	}
	s.vectorX = -1
	s.vectorY = 0
}

func (s *Sprite) GoRight() {
	if s.vectorX == 1 {
		return // going right already
	}

	if s.vectorX == -1 {
		return // prevent suicide
	}
	s.vectorX = 1
	s.vectorY = 0
}

func (s *Sprite) SpeedUp() {
	if s.speed >= 1 {
		return // max speed reached
	}
	s.speed += 0.01
}

func (s *Sprite) HasCollisionWith(c XYCoordinate) bool {
	return c.IsEqual(*s.GetCoordinate())
}

func (s *Sprite) Move(width, height int) {
	s.forward(width, height)
}

func (s *Sprite) GetCoordinate() *XYCoordinate {
	return NewXyCoordinate(int(s.x), int(s.y))
}

func (s *Sprite) GetDistanceTo(width, height int, c XYCoordinate) (int, int) {
	return s.GetCoordinate().GetDistanceXYTo(c)
}

func (s *Sprite) GetDistanceCrossBorders(width, height int, c XYCoordinate) (dX, dY int) {
	if width+height == 0 {
		return
	}

	sC := s.GetCoordinate()

	// sprite distance to map center and border
	sDCX, sDCY := s.getDistanceToMapCenter(width, height)
	sDBX := sC.GetDistanceToNearestXBorder(width)
	sDBY := sC.GetDistanceToNearestYBorder(height)
	// target point distance to map center and border
	tDCX, tDCY := c.getDistanceToMapCenter(width, height)
	tDBX := c.GetDistanceToNearestXBorder(width)
	tDBY := c.GetDistanceToNearestYBorder(height)

	dTCX := sDCX + tDCX // distance trough center X
	dTBX := sDBX + tDBX
	dTCY := sDCY + tDCY // distance trough center y
	dTBY := sDBY + tDBY

	if dTCX < dTBX {
		dX = dTCX
	} else {
		dX = dTBX
	}
	if dTCY < dTBY {
		dY = dTCY
	} else {
		dY = dTBY
	}

	return
}

func (s *Sprite) getDistanceToMapCenter(width, height int) (int, int) {
	return s.GetCoordinate().getDistanceToMapCenter(width, height)
}

func (s *Sprite) forward(width, height int) {
	s.x += s.vectorX * s.speed
	s.y += s.vectorY * s.speed
	s.checkBoundaries(width, height)
}

func (s *Sprite) checkBoundaries(width, height int) {
	if s.x < 0 {
		s.x = float64(width)
	}
	if s.x > float64(width) {
		s.x = 0
	}
	if s.y < 0 {
		s.y = float64(height)
	}
	if s.y > float64(height) {
		s.y = 0
	}
}
