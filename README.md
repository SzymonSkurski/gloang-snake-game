# Gloang snake game
Simple snake game made to practice code skills

## Run

### Linux
```bash
go run .
```

### Windows
```bash
go run main.go app.go snake.go xyCoordinate.go
```
