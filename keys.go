package main

import (
	"fmt"
	"os"

	"github.com/eiannone/keyboard"
)

func keyHandler(app *SnakeApp) {
	keysEvents, err := keyboard.GetKeys(1)
	if err != nil {
		panic(err)
	}
	defer func() {
		_ = keyboard.Close()
		fmt.Println(" Bye!")
		os.Exit(1)
	}()

	for event := range keysEvents {
		if event.Err != nil {
			panic(event.Err)
		}

		if app.HandleKey(event) {
			break
		}
	}
}
