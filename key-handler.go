package main

import "github.com/eiannone/keyboard"

// HandleKey app control, return true to break loop
func (app *SnakeApp) HandleKey(event keyboard.KeyEvent) bool {
	if event.Key == keyboard.KeyEsc {
		return true // break program
	}

	if !app.isInit {
		app.isInit = true
		return false
	}

	if event.Rune == 'r' {
		app.Restart()
		return false
	}

	if event.Rune == 'd' {
		// toggle debug
		app.Debug = !app.Debug
		return false
	}

	switch event.Key {
	case 65514:
		app.snake.GoRight()
	case 65515:
		app.snake.GoLeft()
	case 65516:
		app.snake.Godown()
	case 65517:
		app.snake.GoUp()
	}

	return false
}
