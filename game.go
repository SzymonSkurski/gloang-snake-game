package main

import (
	"time"
)

type Game struct {
	Sprite
	createdAt time.Time
}

func NewGame(c XYCoordinate, vx, vy, s float64) *Game {
	return &Game{
		Sprite:    *NewSprite(float64(c.X), float64(c.Y), vx, vy, s),
		createdAt: time.Now(),
	}
}

func (g *Game) SecondsToGone() int {
	return gameTTL - int(time.Since(g.createdAt).Seconds())
}

func (g *Game) IsGone() bool {
	return time.Since(g.createdAt) > time.Second*gameTTL
}

// IsAboutToGone is 5 s left to gone
func (g *Game) IsAboutToGone() bool {
	return time.Since(g.createdAt) > (time.Second * (gameTTL - 5))
}

// Evade game always try to avoid snake
func (g *Game) Evade(s *Snake, width, height int) {
	if width+height < 1 {
		return
	}

	// are snake nearby
	gc := g.GetCoordinate()
	qw := width / 5
	qh := height / 5
	dx, dy := gc.GetDistanceXYCrossBorders(width, height, *s.GetCoordinate())

	if dx > qw && dy > qh {
		// do nothing
		g.Sprite.Stop()

		return
	}

	// are snake coming towards game
	snakeNextSpot := NewXyCoordinate(int(s.Sprite.x+s.Sprite.vectorX*2), int(s.Sprite.y+s.Sprite.vectorY*2))
	dnx, dny := gc.GetDistanceXYCrossBorders(width, height, *snakeNextSpot)

	// upcoming distance > current distance
	if dnx+dny > dx+dy {
		// do nothing
		g.Sprite.Stop()

		return
	}

	// snake is close game have to escape
	vx := float64(0)
	vy := float64(0)

	// snake is from left
	if snakeNextSpot.X < gc.X {
		vx = 1
	}
	// snake is from right
	if snakeNextSpot.X > gc.X {
		vx = -1
	}
	// snake is from top
	if snakeNextSpot.Y < gc.Y {
		vy = 1
	}
	// snake is from bottom
	if snakeNextSpot.Y > gc.Y {
		vy = -1
	}

	g.Sprite.vectorX = vx
	g.Sprite.vectorY = vy
}
