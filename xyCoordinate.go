package main

type XYCoordinate struct {
	X int
	Y int
}

func NewXyCoordinate(x, y int) *XYCoordinate {
	return &XYCoordinate{
		X: x,
		Y: y,
	}
}

func (c *XYCoordinate) IsEqual(cc XYCoordinate) bool {
	return c.X == cc.X && c.Y == cc.Y
}

func (c *XYCoordinate) XY() (int, int) {
	return c.X, c.Y
}

func (c *XYCoordinate) GetDistanceTo(t XYCoordinate) int {
	x, y := c.GetDistanceXYTo(t)

	return x + y
}

func (c *XYCoordinate) GetDistanceXYTo(t XYCoordinate) (dX, dY int) {
	dX = getDistance(c.X, t.X)
	dY = getDistance(t.Y, c.Y)

	return
}

func (c *XYCoordinate) getDistanceToMapCenter(width, height int) (int, int) {
	if width+height == 0 {
		return 0, 0
	}

	cc := NewXyCoordinate(width/2, height/2)

	return c.GetDistanceXYTo(*cc)
}

func (c *XYCoordinate) getDistanceToRightBorder(width int) int {
	return width - c.X
}

func (c *XYCoordinate) getDistanceToLeftBorder() int {
	return c.X
}

func (c *XYCoordinate) getDistanceToTopBorder() int {
	return c.Y
}

func (c *XYCoordinate) getDistanceToBottomBorder(height int) int {
	return height - c.Y
}

func (c *XYCoordinate) GetDistanceToNearestXBorder(width int) int {
	if width < 1 {
		return 0
	}
	if c.X >= width/2 {
		// distance to right border
		return c.getDistanceToRightBorder(width)
	}

	// distance to left border
	return c.getDistanceToLeftBorder()
}

// GetDistanceToBorder left, right, top, bottom
func (c *XYCoordinate) GetDistanceToBorders(width, height int) (l, r, t, b int) {
	l = c.getDistanceToLeftBorder()
	r = c.getDistanceToRightBorder(width)
	t = c.getDistanceToTopBorder()
	b = c.getDistanceToBottomBorder(height)

	return
}

func (c *XYCoordinate) GetDistanceToNearestYBorder(height int) int {
	if height < 1 {
		return 0
	}
	if c.Y >= height/2 {
		// distance to bottom
		return c.getDistanceToBottomBorder(height)
	}

	// distance to top
	return c.getDistanceToTopBorder()
}

func (c *XYCoordinate) GetDistanceXYCrossBorders(width, height int, t XYCoordinate) (dX, dY int) {
	if width+height == 0 {
		return
	}

	// sprite distance
	sDBLX := c.getDistanceToLeftBorder()
	sDBRX := c.getDistanceToRightBorder(width)
	sDBTY := c.getDistanceToTopBorder()
	sDBBY := c.getDistanceToBottomBorder(height)
	// target point distance
	tDBLX := t.getDistanceToLeftBorder()
	tDBRX := t.getDistanceToRightBorder(width)
	tDBTY := t.getDistanceToTopBorder()
	tDBBY := t.getDistanceToBottomBorder(height)

	distanceX, distanceY := c.GetDistanceXYTo(t) // distance trough center X
	distanceBorderX := sDBLX + tDBRX + 1         // distance trough left - right border
	dTBRX := sDBRX + tDBLX + 1                   // distance trough right border
	dTBTY := sDBTY + tDBBY + 1                   // distance trough border top to bottom
	dTBBY := sDBBY + tDBTY + 1                   // distance trough border bottom to top

	dX = minInt(distanceX, distanceBorderX, dTBRX)
	dY = minInt(distanceY, dTBTY, dTBBY)

	return
}
