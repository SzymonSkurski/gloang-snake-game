package main

import (
	"math"
	"math/rand"
)

// func maxInt(n ...int) (max int) {
// 	l := len(n)
// 	if l == 0 {
// 		return
// 	}

// 	max = n[0]

// 	for i := 1; i < l; i++ {
// 		if n[i] > max {
// 			max = n[i]
// 		}
// 	}

// 	return
// }

func minInt(n ...int) (min int) {
	l := len(n)

	if l == 0 {
		return
	}

	min = n[0]

	for i := 1; i < l; i++ {
		if n[i] < min {
			min = n[i]
		}
	}

	return
}

func getDistance(x1, x2 int) int {
	return int(math.Abs(float64(x1 - x2)))
}

func getRandomInt(max, min int) int {
	return rand.Intn(max-min+1) + min
}

func getRandomVector() int {
	return getRandomInt(1, -1)
}
