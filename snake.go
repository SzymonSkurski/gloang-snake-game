package main

import "fmt"

type Snake struct {
	// x       float64
	// y       float64
	// vectorX float64
	// vectorY float64
	// speed   float64
	Sprite
	Tail   []XYCoordinate
	length int
}

func NewSnake(x, y, vx, vy float64) *Snake {
	s := &Snake{
		Sprite: *NewSprite(x, y, vx, vy, speed),
		length: snakeLength,
	}
	s.Tail = make([]XYCoordinate, s.length-1)
	return s
}

func (s *Snake) DebugMsg() string {
	return fmt.Sprintf("x:%.1f y:%.1f vx:%.0f vy:%.0f s:%.2f", s.x, s.y, s.vectorX, s.vectorY, s.speed)
}

func (s *Snake) Grow() {
	s.length++
}

func (s *Snake) Move(width, height int) {
	s.forward(width, height)
}

func (s *Snake) forward(width, height int) {
	pC := s.getHeadCoordinate()
	s.Sprite.forward(width, height)
	nC := s.getHeadCoordinate()
	if pC.IsEqual(*nC) {
		return
	}
	// have to move tail
	tail := make([]XYCoordinate, s.length-1)
	tail[0] = *pC
	tailEnd := s.Tail[:len(s.Tail)-1]
	for i := 0; i < len(tailEnd); i++ {
		tail[i+1] = tailEnd[i]
	}
	s.Tail = tail
}

func (s *Snake) getHeadCoordinate() *XYCoordinate {
	return s.Sprite.GetCoordinate()
}

func (s *Snake) HasCollisionWithSelf() bool {
	return s.hasCollisionTail(*s.getHeadCoordinate())
}

func (s *Snake) HasCollisionWith(c XYCoordinate) bool {
	return s.Sprite.HasCollisionWith(c) || s.hasCollisionTail(c)
}

func (s *Snake) HasCollisionHead(c XYCoordinate) bool {
	return c.IsEqual(*s.getHeadCoordinate())
}

func (s *Snake) hasCollisionTail(c XYCoordinate) bool {
	// check tail
	for _, tp := range s.Tail {
		if tp.IsEqual(c) {
			return true // tail has collision
		}
	}
	return false
}
